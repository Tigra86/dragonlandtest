﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private int _damage;

    private bool _reachEnemy;
    private float _shootDistance;

    private void OnEnable()
    {
        _reachEnemy = false;
    }

    private void Update()
    {
        if (Vector2.Distance(transform.parent.position, transform.position) < _shootDistance)
            transform.Translate(Vector3.right * _speed * Time.deltaTime);
        else
            Die();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (_reachEnemy == false)
        {
            _reachEnemy = true;
            if (collision.gameObject.TryGetComponent(out Enemy enemy))
            {
                enemy.ApplyDamage(_damage);
                gameObject.SetActive(false);
            }
        }
    }

    private void Die()
    {
        gameObject.SetActive(false);
    }

    public void SetShootDistavce(float shootDistance)
    {
        _shootDistance = shootDistance;
    }
}