﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class ShootDefender : Defender
{
    [Header("Attributtes")]
    [SerializeField] private Bullet _bullet;
    [SerializeField] private float _delay;
    [SerializeField] private float _shootDistance;
    [SerializeField] private int _capacityPool;

    [Header("Unity Setup Fields")]
    [SerializeField] private Transform _shootPoint;
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private ContactFilter2D _filter;

    public float ShootDistance => _shootDistance;

    private float _timeLastShoot;
    private readonly RaycastHit2D[] _results = new RaycastHit2D[10];
    private ObjectPool _pool = new ObjectPool();
    private Animator _animator;

    private void OnEnable()
    {
        _pool.Initialize(_bullet.gameObject, _shootPoint.gameObject, _capacityPool);
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (NeedShoot())
        {
            if (_timeLastShoot <= 0)
            {
                _animator.SetTrigger("Attack");
                _timeLastShoot = _delay;
            }
            _timeLastShoot -= Time.deltaTime;
        }
    }

    private bool NeedShoot()
    {
        return _rigidbody.Cast(Vector2.right, _filter, _results, _shootDistance) > 0;
    }

    private void Shoot()
    {
        if (_pool.TryGetObject(out GameObject bullet))
        {
            SetBullet(bullet, _shootPoint.position);
        }
    }

    private void SetBullet(GameObject bullet, Vector2 position)
    {
        bullet.SetActive(true);
        bullet.transform.position = position;
        bullet.GetComponent<Bullet>().SetShootDistavce(_shootDistance);
    }
}