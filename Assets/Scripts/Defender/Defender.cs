﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Defender : MonoBehaviour
{
    [SerializeField] private int _cost;
    [SerializeField] private Sprite _icon;
    [SerializeField] private float _cooldown;
    [SerializeField] private int _health;

    public int Cost => _cost;
    public Sprite Icon => _icon;
    public int Health => _health;
    public float Cooldown => _cooldown;

    public event UnityAction Died;

    public void ApplyDamage(int damage)
    {
        _health -= damage;

        if (_health <= 0)
        {
            Destroy(gameObject);
            Died?.Invoke();
        }
    }
}
