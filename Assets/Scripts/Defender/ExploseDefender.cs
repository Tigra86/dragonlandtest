﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class ExploseDefender : Defender
{
    [SerializeField] private float _radius;
    [SerializeField] private float _delay;

    private float _currentTime;
    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (_currentTime >= _delay)
        {
            _animator.Play("Explose");
            _currentTime = 0;
        }
        _currentTime += Time.deltaTime;
    }

    private void Explose()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, _radius);

        foreach (var collider in colliders)
            if (collider.gameObject.TryGetComponent(out Enemy enemy))
                enemy.ApplyDamage(enemy.Health);
    }
}