﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlePlaceCreater : MonoBehaviour
{
    [SerializeField] private int _rowCount;
    [SerializeField] private int _columnCount;
    [SerializeField] private float _scale;
    [SerializeField] private float _spaceBetweenNodes;
    [SerializeField] private Vector3 _startPoint;
    [SerializeField] private GameObject _nodeTemplate;
    [SerializeField] private GameObject _highlightPanel;

    public int RowCount => _rowCount;
    public int ColumnCount => _columnCount;
    public float Scale => _scale;
    public float SpaceBetweenNodes => _spaceBetweenNodes;
    public float StartY { get { return _startPoint.y; } }
    public float StartX { get { return _startPoint.x; } }

    private Vector3 _currentPoint;

    private void OnEnable()
    {
        _currentPoint = _startPoint;
        Create();
    }

    private void Create()
    {
        for (int i = 0; i < _rowCount; i++)
        {
            for (int j = 0; j < _columnCount; j++)
            {
                var node = Instantiate(_nodeTemplate, _currentPoint, Quaternion.identity);
                node.transform.localScale = new Vector3(_scale, _scale, 1);
                node.transform.parent = transform;
                node.GetComponent<Node>().InitHighlightPanel(_highlightPanel);
                _currentPoint.x += _scale + _spaceBetweenNodes;
            }
            _currentPoint.x = _startPoint.x;
            _currentPoint.y -= _scale + _spaceBetweenNodes;
        }
    }
}
