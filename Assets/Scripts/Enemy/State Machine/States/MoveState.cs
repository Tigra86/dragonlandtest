﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class MoveState : State
{
    [SerializeField] private float _speed;

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        _animator.Play("Walk");
    }

    private void OnDisable()
    {
        _animator.StopPlayback();
    }

    private void Update()
    {
        transform.Translate(Vector2.left * Time.deltaTime * _speed);
    }
}
