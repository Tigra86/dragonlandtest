﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator), typeof(AttackTransition))]
public class AttackState : State
{
    [SerializeField] private int _damage;
    [SerializeField] private float _delay;

    private Animator _animator;
    private float _timeLastAttack;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        _timeLastAttack = 0.25f;
    }

    private void Update()
    {
        if (_timeLastAttack <= 0)
        {
            Attack(TargetDefender);
            _timeLastAttack = _delay;
        }

        _timeLastAttack -= Time.deltaTime;
    }

    private void Attack(Defender defender)
    {
        _animator.Play("Attack");
        if (defender != null)
            defender.ApplyDamage(_damage);
    }
}
