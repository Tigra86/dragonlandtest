﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DieState : State
{
    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        _animator.Play("Die");
        Destroy(gameObject, 0.5f);
    }

    private void OnDisable()
    {
        _animator.StopPlayback();
    }
}

