﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State : MonoBehaviour
{
    [SerializeField] private List<Transition> _transitions;

    public PlayerData PlayerData { get; private set; }
    protected Defender TargetDefender;

    public void Enter(PlayerData playerData, Defender defender)
    {

        if (enabled == false)
        {
            PlayerData = playerData;
            TargetDefender = defender;
            enabled = true;

            foreach (var transition in _transitions)
            {
                transition.enabled = true;
                transition.Init(playerData, defender);
            }
        }
    }

    public State GetNextState(PlayerData playerData, ref Defender TargetDefender)
    {
        foreach (var transition in _transitions)
        {
            if (transition.NextTransit)
            {
                transition.TryGetCollisionDefender(ref TargetDefender);
                return transition.TargetState;
            }
        }
        return null;
    }

    public void Exit()
    {
        if (enabled == true)
        {
            foreach (var transition in _transitions)
                transition.enabled = false;

            enabled = false;
        }
    }
}