﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ReachFinishTransition : Transition
{
    public event UnityAction<Enemy> FinishReached;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent(out FinishBorder finish))
        {
            NextTransit = true;
            FinishReached?.Invoke(GetComponent<Enemy>());
        }
    }
}
