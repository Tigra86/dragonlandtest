﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttackTransition : Transition
{
    [SerializeField] private float _attackDistance;
    [SerializeField] private ContactFilter2D _filter;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent(out CollisionDefender))
        {  
            NextTransit = true;
            Vector3 newXPosition = new Vector3(transform.position.x + Random.Range(-0.4f, 0.4f), transform.position.y, 0);
            GetComponent<Enemy>().gameObject.transform.position = newXPosition;
        }
    }
}