﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Transition : MonoBehaviour
{
    [SerializeField] private State _targetState;

    protected PlayerData PlayerData;
    protected Defender CollisionDefender;
    protected Defender TargetDefender;

    public State TargetState => _targetState;
    public bool NextTransit { get; protected set; }

    public void Init(PlayerData playerData, Defender defender)
    {
        PlayerData = playerData;
        TargetDefender = defender;
    }

    private void OnEnable()
    {
        NextTransit = false;
    }

    public bool TryGetCollisionDefender(ref Defender collisionDefender)
    {
        collisionDefender = CollisionDefender;
        return collisionDefender != null;
    }
}