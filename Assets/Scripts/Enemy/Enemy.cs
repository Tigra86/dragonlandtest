﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
public class Enemy : MonoBehaviour
{
    [SerializeField] private int _health;
    [SerializeField] private int _reward;
    [SerializeField] private GameObject _templateReward;
    [SerializeField] private float _verticalOffset;

    private PlayerData _playerData;
    private Animator _animator;

    public event UnityAction<Enemy> Died;
    public PlayerData PlayerData => _playerData;
    public int Reward => _reward;
    public int Health => _health;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        Vector3 newPosition = new Vector3(transform.position.x, transform.position.y + _verticalOffset, 0);
        transform.position = newPosition;
    }

    public void ApplyDamage(int damage)
    {
        _health -= damage;

        if (_health <= 0)
        {
            if (_templateReward != null)
                Instantiate(_templateReward, transform.position, Quaternion.identity);
            Died?.Invoke(this);
        }

        StartCoroutine(DoDamageColor());
    }

    private IEnumerator DoDamageColor()
    {
        Color current = GetComponent<SpriteRenderer>().color;
        float colorG = GetComponent<SpriteRenderer>().color.g;
        float colorB = GetComponent<SpriteRenderer>().color.b;
        float speed = 5;

        float currentValueG = colorG;
        float currentValueB = colorB;
        while (currentValueG > 0f)
        {
            currentValueG = Mathf.MoveTowards(currentValueG, 0, Time.deltaTime * speed);
            currentValueB = Mathf.MoveTowards(currentValueB, 0, Time.deltaTime * speed);
            current.g = currentValueG;
            current.b = currentValueB;
            GetComponent<SpriteRenderer>().color = current;
            yield return null;
        }
        while (currentValueG < colorG)
        {
            currentValueG = Mathf.MoveTowards(currentValueG, colorG, Time.deltaTime * speed);
            currentValueB = Mathf.MoveTowards(currentValueB, colorB, Time.deltaTime * speed);
            current.g = currentValueG;
            current.b = currentValueB;
            GetComponent<SpriteRenderer>().color = current;
            yield return null;
        }
    }

    public void Init(PlayerData target)
    {
        _playerData = target;
    }
}