﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObjectPool : MonoBehaviour
{
    private List<GameObject> _pool = new List<GameObject>();

    public void Initialize(GameObject prefab, GameObject _contaner, int _capacity)
    {
        for (int i = 0; i < _capacity; i++)
        {
            var objectPool = Instantiate(prefab, _contaner.transform);
            objectPool.SetActive(false);

            _pool.Add(objectPool);
        }
    }

    protected void Initialize(GameObject[] prefabs, GameObject _contaner, int _capacity)
    {
        for (int i = 0; i < _capacity; i++)
        {
            var randomNumberPrefab = Random.Range(0, prefabs.Length);
            var objectPool = Instantiate(prefabs[randomNumberPrefab], _contaner.transform);
            objectPool.SetActive(false);

            _pool.Add(objectPool);
        }
    }

    public bool TryGetObject(out GameObject result)
    {
        result = _pool.FirstOrDefault(t => t.activeSelf == false);
        return result != null;
    }
}