﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class Spawner : MonoBehaviour
{
    [SerializeField] private List<Wave> _waves;
    [SerializeField] private Vector2[] _spawnPoints;
    [SerializeField] private PlayerData _playerData;
    [SerializeField] private float _timeBeforeWave;
    [SerializeField] private BattlePlaceCreater _battleCreater;
    [SerializeField] private GameObject _winPanel;
    [SerializeField] private LevelData _levelData;

    public event UnityAction<int, int> OnEnemyCreated;
    public int DiedEnemies { get; private set; }

    private Wave _currentWave;
    private int _currentWaveNumber = 0;
    private float _timeAfterLastSpawn;
    private float _delay;
    private int _spawned;
    private int _spawnedAllWaves = 1;
    private bool _isSpawn;

    private void OnEnable()
    {
        SetSpawnPoints();
        _isSpawn = true;
        SetWave(_currentWaveNumber);
        _delay = _currentWave.StartDelay;
        Time.timeScale = 1;
        DiedEnemies = 0;
    }

    private void Update()
    {
        if (_isSpawn == false)
            return;

        if (_currentWave == null)
            return;

        _timeAfterLastSpawn += Time.deltaTime;

        if (_timeAfterLastSpawn >= _delay)
        {
            InstantiateEnemy();
            _spawned++;
            _spawnedAllWaves++;
            _timeAfterLastSpawn = 0;
            _delay = _currentWave.InnerDelay;
        }

        if (_spawned >= _currentWave.Count)
        {
            if (_waves.Count > _currentWaveNumber + 1)
            {
                NextWave();
                _delay = _currentWave.StartDelay;
            }
            else
            {
                _currentWave = null;
            }
        }
    }

    public void InstantiateEnemy()
    {
        Vector2 spawnPoint = GetRandomSpawnPoint();
        Enemy enemy = Instantiate(_currentWave.Template, spawnPoint, Quaternion.identity, transform).GetComponent<Enemy>();
        enemy.Init(_playerData);
        OnEnemyCreated?.Invoke(_spawnedAllWaves, GetCountAllEnemies());
        enemy.Died += OnEnemyDiying;
    }

    private Vector2 GetRandomSpawnPoint()
    {
        return _spawnPoints[Random.Range(0, _spawnPoints.Length)];
    }

    private void SetWave(int number)
    {
        _currentWave = _waves[number];
    }

    private void NextWave()
    {
        SetWave(++_currentWaveNumber);
        _spawned = 0;
    }

    private void OnEnemyDiying(Enemy enemy)
    {
        enemy.Died -= OnEnemyDiying;
        _playerData.AddMoney(enemy.Reward);
        DiedEnemies++;
        if (DiedEnemies == GetCountAllEnemies())
        {
            FinishLevel();
        }
    }

    private int GetCountAllEnemies()
    {
        int count = 0;
        foreach (var wave in _waves)
        {
            count += wave.Count;
        }
        return count;
    }

    private void SetSpawnPoints()
    {
        int pointsCount = _battleCreater.RowCount;
        _spawnPoints = new Vector2[pointsCount];

        float currentY = _battleCreater.StartY;

        for (int i = 0; i < pointsCount; i++)
        {
            Vector2 currentPosition = new Vector2(transform.position.x, currentY);
            _spawnPoints[i] = currentPosition;
            currentY -= _battleCreater.Scale + _battleCreater.SpaceBetweenNodes;
        }
    }

    private void FinishLevel()
    {
        StartCoroutine(ShowWinPanel());
        if (SceneManager.GetActiveScene().buildIndex < _levelData.MaxAvailableLevel)
            _levelData.IncreaseCurrentLevel();
    }

    private IEnumerator ShowWinPanel()
    {
        yield return new WaitForSeconds(2);
        _winPanel.SetActive(true);
    }
}

[System.Serializable]
public class Wave
{
    public GameObject Template;
    public float StartDelay;
    public float InnerDelay;
    public int Count;
}