﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MoneyView : MonoBehaviour
{
    [SerializeField] private TMP_Text _money;
    [SerializeField] private PlayerData _playerData;

    private void OnEnable()
    {
        _playerData.MoneyChanged += Render;
    }

    private void OnDisable()
    {
        _playerData.MoneyChanged -= Render;
    }

    public void Render(int money)
    {
        _money.text = money.ToString();
    }
}
