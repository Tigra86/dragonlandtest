﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DefenderSelection : MonoBehaviour
{
    [SerializeField] private List<Defender> _defenders;
    [SerializeField] private DefenderView _template;
    [SerializeField] private Transform _container;

    private List<DefenderView> _views;

    public event UnityAction<DefenderView> ViewAdded;

    private void Start()
    {
        _views = new List<DefenderView>();

        foreach (var defender in _defenders)
            AddDefender(defender);
    }

    private void AddDefender(Defender defender)
    {
        var view = Instantiate(_template, _container);
        view.Render(defender);
        _views.Add(view);
        ViewAdded?.Invoke(view);
    }

    public void ResetAllSelections()
    {
        foreach (var view in _views)
            if (view.IsSelected == true)
                view.SetEnableState();
    }
}
