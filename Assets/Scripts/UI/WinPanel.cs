﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinPanel : MonoBehaviour
{
    [SerializeField] private Spawner _spawner;
    [SerializeField] private GameObject _panel;
    [SerializeField] private Button _continue;
    [SerializeField] private Button _closeIcon;
    [SerializeField] private LevelData _levelData;

    private void OnEnable()
    {
        _continue.onClick.AddListener(OnClickContinue);
        _closeIcon.onClick.AddListener(OnClickCloseIcon);
        Time.timeScale = 0;
    }

    private void OnClickContinue()
    {
        LoadNextScene();
    }

    private void LoadNextScene()
    {
        var currentSceneNumber = SceneManager.GetActiveScene().buildIndex;
        if (currentSceneNumber < _levelData.MaxAvailableLevel)
            SceneManager.LoadScene(currentSceneNumber + 1);
        else
            OnClickCloseIcon();
    }

    private void OnClickCloseIcon()
    {
        SceneManager.LoadScene(0);
    }
}
