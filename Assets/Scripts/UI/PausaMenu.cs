﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PausaMenu : MonoBehaviour
{
    [SerializeField] private Button _menu;
    [SerializeField] private GameObject _menuPanel;
    [SerializeField] private Button _continue;
    [SerializeField] private Button _restart;
    [SerializeField] private Button _mainMenu;
    [SerializeField] private CanvasGroup _battleUiGroup;

    private void Start()
    {
        _menuPanel.SetActive(false);
        _menu.onClick.AddListener(OpenPauseMenu);
        _continue.onClick.AddListener(Continue);
        _restart.onClick.AddListener(Restart);
        _mainMenu.onClick.AddListener(OpenMainMenu);
    }

    public void OpenPauseMenu()
    {
        _menuPanel.SetActive(true);
        Time.timeScale = 0;
        _battleUiGroup.alpha = 0.3f;
        _battleUiGroup.interactable = false;
    }

    private void Continue()
    {
        Time.timeScale = 1;
        _menuPanel.SetActive(false);
        _battleUiGroup.alpha = 1f;
        _battleUiGroup.interactable = true;
    }

    private void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OpenMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
