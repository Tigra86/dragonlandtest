﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(MainMenu))]
public class LevelsMenu : MonoBehaviour
{
    [SerializeField] private Button _crossButton;
    [SerializeField] private List<Level> _levels;
    [SerializeField] private LevelView _template;
    [SerializeField] private GameObject _itemContainer;

    private MainMenu _mainMenu;

    private void OnEnable()
    {
        _mainMenu = GetComponent<MainMenu>();
        _crossButton.onClick.AddListener(GoBack);

        for (int i = 0; i < _levels.Count; i++)
            AddItem(_levels[i]);
    }

    public void GoBack()
    {
        _mainMenu.OpenMainMenuPanel();
    }

    private void AddItem(Level level)
    {
        var view = Instantiate(_template, _itemContainer.transform);
        view.Render(level);
    }
}