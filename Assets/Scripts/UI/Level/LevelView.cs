﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class LevelView : MonoBehaviour
{
    [SerializeField] private Image _icon;
    [SerializeField] private TMP_Text _levelText;
    [SerializeField] private TMP_Text _levelNumberText;
    [SerializeField] private Button _levelButton;
    [SerializeField] private Color _enableColorIcon;
    [SerializeField] private Color _disableColorIcon;
    [SerializeField] private LevelData _levelData;

    private Level _level;

    private void OnEnable()
    {
        _levelButton.onClick.AddListener(delegate { OpenLevel(_level.LevelNumber); });
    }

    public void OpenLevel(int number)
    {
        SceneManager.LoadScene(number);
    }

    private void TryDoEnable()
    {
        if (_level != null)
            _levelButton.interactable = _level.LevelNumber <= _levelData.CurrentLevel; 

        if (_levelButton.interactable == true)
            _icon.color = _enableColorIcon;
        else
            _icon.color = _disableColorIcon;
    }

    private void TryDoLock()
    {
        if (_level == null)
            return;

        if (_level.IsLocked)
        {
            _levelText.text = string.Empty;
            _levelNumberText.text = string.Empty;
        }
    }

    public void Render(Level level)
    {
        _level = level;
        _icon.sprite = level.Icon;
        _levelNumberText.text = level.LevelNumber.ToString();
        _levelButton.interactable = level.IsEnable;

        TryDoEnable();
        TryDoLock();
    }
}
