﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
    [SerializeField] private int _levelNumber;
    [SerializeField] private Sprite _icon;
    [SerializeField] private bool _isEnable = false;
    [SerializeField] private bool _isLocked = false;

    public int LevelNumber => _levelNumber;
    public Sprite Icon => _icon;
    public bool IsEnable => _isEnable;
    public bool IsLocked => _isLocked;
}
