﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private GameObject _messagesPanel;
    [SerializeField] private Spawner _spawner;
    [SerializeField] private GameObject _highlightPanel;
    [SerializeField] private DefenderSelection _selection;
    [SerializeField] private DefenderBuilder _builder;
    [SerializeField] private GameObject _pointer;
    [SerializeField] private CanvasGroup _group;
    [SerializeField] private Animator _blick;

    private DefenderView _view;
    private TMP_Text _message;
    private string _highlightText;
    private string _defenderBuiltText;
    private string _defenderMoreBuiltText;
    private bool _isShowNextMessage;


    private void OnEnable()
    {
        _spawner.gameObject.SetActive(false);
        _highlightPanel.SetActive(false);
        _messagesPanel.gameObject.SetActive(true);
        _pointer.SetActive(true);
        _blick.Play("Blick");
        _isShowNextMessage = true;

        _selection.ViewAdded += OnViewAdded;
        _builder.DefenderBuilt += OnDefenderBuilt;

        _message = _messagesPanel.GetComponentInChildren<TMP_Text>();
        _highlightText = "Tap on the ground to put the defender!";
        _defenderBuiltText = "Nicely done!";
        _defenderMoreBuiltText = "Now you're having enough money to add more one defender!";

        Time.timeScale = 1;
    }

    private void OnDisable()
    {
        if (_view != null)
            _view.IconClicked -= OnIconClicked;
        _messagesPanel.SetActive(false);

        _selection.ViewAdded -= OnViewAdded;
    }

    private void OnViewAdded(DefenderView view)
    {
        _view = view;
        _view.IconClicked += OnIconClicked;
    }

    private void OnIconClicked()
    {
        HighlightLine();
        _pointer.SetActive(false);
        _blick.Play("Idle");
    }

    private void HighlightLine()
    {
        _highlightPanel.SetActive(true);
        _message.text = _highlightText;
    }

    private void OnDefenderBuilt()
    {
        _highlightPanel.gameObject.SetActive(false);
        _spawner.gameObject.SetActive(true);

        _message.text = _defenderBuiltText;
        StartCoroutine(TurnOffTutorial());
        _builder.DefenderBuilt -= OnDefenderBuilt;

        if (_isShowNextMessage)
            _builder.PlayerData.MoneyChanged += ShowMessage;
    }

    private void ShowMessage(int money)
    {
        _isShowNextMessage = false;
        _builder.PlayerData.MoneyChanged -= ShowMessage;
        _builder.DefenderBuilt += OnDefenderBuilt;
        _pointer.SetActive(true);
        _blick.Play("Blick");

        _view.IconClicked += OnIconClicked;
        _message.text = _defenderMoreBuiltText;
        _messagesPanel.SetActive(true);

        StartCoroutine(TurnOffTutorial());
    }

    private IEnumerator TurnOffTutorial()
    {
        yield return new WaitForSeconds(4);
        _messagesPanel.SetActive(false);
    }
}
