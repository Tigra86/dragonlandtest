﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject _basicPanel;
    [SerializeField] private GameObject _levelsPanel;
    [SerializeField] private Button _adventure;
    [SerializeField] private Button _exit;
    [SerializeField] private LevelData _levelData;

    private void OnEnable()
    {
        OpenMainMenuPanel();
        _adventure.onClick.AddListener(OpenLevelsPanel);
        _exit.onClick.AddListener(Quit);
        TryOpenLevelsPanel();
    }

    private void OnDisable()
    {
        _adventure.onClick.RemoveListener(OpenLevelsPanel);
    }

    private void TryOpenLevelsPanel()
    {
        if (_levelData.CurrentLevel > 1)
            OpenLevelsPanel();
    }

    private void OpenPanel(GameObject panel)
    {
        panel.SetActive(true);
    }

    private void ClosePanel(GameObject panel)
    {
        panel.SetActive(false);
    }

    public void OpenLevelsPanel()
    {
        OpenPanel(_levelsPanel);
        ClosePanel(_basicPanel);
    }

    public void OpenMainMenuPanel()
    {
        OpenPanel(_basicPanel);
        ClosePanel(_levelsPanel);
    }

    private void Quit()
    {
        Application.Quit();
    }
}