﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    [SerializeField] private Slider _slider;
    [SerializeField] private Spawner _spawner;

    private Coroutine _currentValueChange;

    private void OnEnable()
    {
        _spawner.OnEnemyCreated += OnValeChanged;
    }

    private void OnDisable()
    {
        _spawner.OnEnemyCreated -= OnValeChanged;
    }

    private void OnValeChanged(int value, int maxValue)
    {
        if (_currentValueChange != null)
            StopCoroutine(_currentValueChange);

        _currentValueChange = StartCoroutine(ChangeProgress(value, maxValue));
    }

    private IEnumerator ChangeProgress(int value, int maxValue)
    {
        float currentValue = _slider.value;
        float endValue = (float)value / maxValue;

        while (currentValue < endValue)
        {
            currentValue = Mathf.MoveTowards(currentValue, endValue, 0.00075f);
            _slider.value = currentValue;
            yield return null;
        }
        yield break;
    }
}