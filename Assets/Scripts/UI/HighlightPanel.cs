﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightPanel : MonoBehaviour
{
    [SerializeField] private BattlePlaceCreater _battleCreater;

    private float _hight;
    private float _width;
    private float _positionY;

    private void Start()
    {
        _positionY = _battleCreater.StartY;
        SetScale();
        SetPosition();
    }

    private void SetScale()
    {
        var scale = _battleCreater.Scale;
        _hight = scale;

        var columnAnoumt = _battleCreater.ColumnCount;
        var spaceBetween = _battleCreater.SpaceBetweenNodes;
        _width = scale * columnAnoumt + spaceBetween * (columnAnoumt - 1);

        var localScale  = new Vector3(_width, _hight + 0.1f, 1);
        transform.localScale = localScale;
    }

    private void SetPosition()
    {
        var newPosition = new Vector3(_battleCreater.StartX - _hight / 2 + _width / 2, _positionY, 1);
        transform.position = newPosition;
    }

    public void SetPositionY(float positionY)
    {
        _positionY = positionY;
        SetPosition();
    }
}
