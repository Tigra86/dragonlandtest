﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class GameoverPanel : MonoBehaviour
{
    [SerializeField] private Button _yes;
    [SerializeField] private Button _closeIcon;
    [SerializeField] private FinishBorder _finishBorder;
    [SerializeField] private GameObject _battleUiElements;

    private CanvasGroup _gameoverPanel;
    private CanvasGroup _battleUiGroup;

    private void OnEnable()
    {
        _yes.onClick.AddListener(OnClickYesButton);
        _closeIcon.onClick.AddListener(OnClickCloseIcon);
        _finishBorder.ReachedFinish += OnReachFinish;
    }

    private void Start()
    {
        _gameoverPanel = GetComponent<CanvasGroup>();
        _gameoverPanel.alpha = 0;
        _gameoverPanel.interactable = false;
        _battleUiGroup = _battleUiElements.GetComponent<CanvasGroup>();
        _battleUiGroup.alpha = 1;
        _battleUiGroup.interactable = true;
    }

    private void OnClickYesButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }

    private void OpenMainMenuScene()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }

    private void OnClickCloseIcon()
    {
        OpenMainMenuScene();
    }

    private void OnReachFinish()
    {
        _gameoverPanel.alpha = 1;
        Time.timeScale = 0;
        _battleUiGroup.alpha = 0.5f;
        _battleUiGroup.interactable = false;
        _gameoverPanel.interactable = true;
    }
}
