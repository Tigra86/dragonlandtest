﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class DefenderView : MonoBehaviour
{
    [SerializeField] private Image _icon;
    [SerializeField] private TMP_Text _cost;
    [SerializeField] private Button _selectButton;
    [SerializeField] private Image _cooldownImage;
    [SerializeField] private Image _disableImage;
    [SerializeField] private Image _selectImage;

    private DefenderBuilder _builder;
    private Defender _defender;
    private DefenderSelection _selection;

    public bool IsSelected { get; private set; }
    public bool IsBuilding { get; private set; }
    public event UnityAction IconClicked;

    private void OnEnable()
    {
        _builder = DefenderBuilder.Instance;
        _selectButton.onClick.AddListener(OnClickIcon);
        _builder.PlayerData.MoneyChanged += CheckMoney;
        _builder.DefenderBuilt += OnDefenderBuilt;
        _selection = GetComponentInParent<DefenderSelection>();

        SetEnableState();
    }

    private void OnDisable()
    {
        _selectButton.onClick.RemoveListener(OnClickIcon);
        _builder.DefenderBuilt -= OnDefenderBuilt;
        _builder.PlayerData.MoneyChanged -= CheckMoney;
    }

    private void Start()
    {
        CheckMoney(_builder.PlayerData.Money);
    }

    public void Render(Defender defender)
    {
        _defender = defender;
        _icon.sprite = defender.Icon;
        _cost.text = defender.Cost.ToString();
    }

    private void OnClickIcon()
    {
        IconClicked?.Invoke();

        if (_builder.PlayerData.Money < _defender.Cost)
            return;

        if (IsSelected == false)
        {
            SetSelectState();
            IsSelected = true;
        }
        else
        {
            SetEnableState();
            IsSelected = false;
        }
    }

    public void SetEnableState()
    {
        IsSelected = false;
        _selectButton.interactable = true;
        if (_defender == _builder.DefenderToBuild)
            _builder.SetDefenderToBuild(null);
        _cooldownImage.gameObject.SetActive(false);
        _disableImage.gameObject.SetActive(false);
        _selectImage.gameObject.SetActive(false);
    }

    public void SetSelectState()
    {
        _selection.ResetAllSelections();
        IsSelected = true;
        _selectButton.interactable = true;
        _builder.SetDefenderToBuild(_defender);
        _builder.PlayerData.MoneyChanged -= CheckMoney;
        _cooldownImage.gameObject.SetActive(false);
        _disableImage.gameObject.SetActive(false);
        _selectImage.gameObject.SetActive(true);
    }

    private void SetDisableState()
    {
        IsSelected = false;
        _selectButton.interactable = false;
        _cooldownImage.fillAmount = 0;
        _cooldownImage.gameObject.SetActive(true);
        _disableImage.gameObject.SetActive(true);
        _selectImage.gameObject.SetActive(false);
        _builder.SetDefenderToBuild(null);
    }

    private void OnDefenderBuilt()
    {
        if (IsSelected)
        {
            SetDisableState();
            StartCoroutine(StartCooldown());
            CheckMoney(_builder.PlayerData.Money);
        }
    }

    private IEnumerator StartCooldown()
    {
        IsBuilding = true;
        float currentValue = _defender.Cooldown;
        while (currentValue > 0f)
        {
            currentValue = Mathf.MoveTowards(currentValue, 0, Time.deltaTime);
            _cooldownImage.fillAmount = currentValue / _defender.Cooldown;
            yield return null;
        }
        _builder.PlayerData.MoneyChanged += CheckMoney;
        IsBuilding = false;
        SetEnableState();
        CheckMoney(_builder.PlayerData.Money);
        yield break;
    }

    private void CheckMoney(int money)
    {
        if (money < _defender.Cost)
            SetDisableState();
        else
            if (IsBuilding == false)
            SetEnableState();
    }
}
