﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PlayerData))]
public class DefenderBuilder : MonoBehaviour
{
    public static DefenderBuilder Instance;
    
    public PlayerData PlayerData { get; private set; }
    public bool CanBuild { get { return _defenderToBuild != null; } }
    public bool IsMoneyEnoughToBuild { get { return PlayerData.Money >= _defenderToBuild.Cost; } }
    public  Defender DefenderToBuild => _defenderToBuild;

    public event UnityAction DefenderBuilt;

    private Defender _defenderToBuild;

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.Log("More than one DefenderBuilder in the scene.");
            return;
        }
        Instance = this;
    }

    private void OnEnable()
    {
        PlayerData = GetComponent<PlayerData>();
    }

    public void SetDefenderToBuild(Defender defender)
    {
        _defenderToBuild = defender;
    }

    public void BuildDefenderOn(Node node)
    {
        Vector3 positionToBuild = new Vector3(node.transform.position.x, node.transform.position.y - 0.8f);
        var defender = Instantiate(_defenderToBuild, positionToBuild, Quaternion.identity, node.transform);

        defender.Died += node.OnDefenderDied;
        node.SetIsBusy(true);
        PlayerData.AddMoney(-_defenderToBuild.Cost);

        DefenderBuilt?.Invoke();
    }
}