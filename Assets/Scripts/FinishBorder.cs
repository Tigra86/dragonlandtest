﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FinishBorder : MonoBehaviour
{
    [SerializeField] private LevelsMenu _levelsMenu;

    public event UnityAction ReachedFinish;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent(out Enemy enemy))
        {
            ReachedFinish?.Invoke();
        }
    }
}