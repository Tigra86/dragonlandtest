﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerData : MonoBehaviour
{
    [SerializeField] private int _startMoney;

    public int Money { get; private set; }
    public event UnityAction<int> MoneyChanged;

    public void AddMoney(int money)
    {
        Money += money;
        MoneyChanged?.Invoke(Money);
    }

    private void Start()
    {
        Money = _startMoney;
        MoneyChanged?.Invoke(Money);
    }
}
