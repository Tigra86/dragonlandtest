﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Crystal : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _changeTime;

    private SpriteRenderer _sprite;
    private Color _color;

    private void Start()
    {
        _sprite = GetComponent<SpriteRenderer>();
        _color = new Color(1, 1, 1, 0);
        StartCoroutine(SetColor(_color));
    }

    private void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * _speed);
    }

    private IEnumerator SetColor(Color color)
    {
        float currentTime = _changeTime;
        while (currentTime > 0f)
        {
            currentTime = Mathf.MoveTowards(currentTime, 0, Time.deltaTime);
            color.a = currentTime / _changeTime;
            _sprite.color = color;
            yield return null;
        }
        Destroy(gameObject);
        yield break;
    }
}
