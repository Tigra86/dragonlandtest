﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Node : MonoBehaviour
{
    [SerializeField] private Color _hoverColor;
    
    private SpriteRenderer _renderer;
    private Color _startColor;
    private DefenderBuilder _builder;
    private GameObject _highlightPanel;
    private bool _isBusy;

    private void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _startColor = _renderer.color;
        _builder = DefenderBuilder.Instance;
        _isBusy = false;
    }

    private void OnMouseEnter()
    {
        if (_builder == null)
            return;

        if (_builder.CanBuild == false)
            return;

        if (_isBusy)
            return;

        if (_builder.IsMoneyEnoughToBuild == false)
            return;

        _renderer.color = _hoverColor;
        _highlightPanel.GetComponent<HighlightPanel>().SetPositionY(transform.position.y);
        _highlightPanel.SetActive(true);
    }

    private void OnMouseExit()
    {
        _renderer.color = _startColor;
        _highlightPanel.SetActive(false);
    }

    private void OnMouseDown()
    {
        if (_builder == null)
            return;

        if (_builder.CanBuild == false)
            return;

        if (_isBusy)
            return;

        if (_builder.IsMoneyEnoughToBuild == false)
            return;

        _builder.BuildDefenderOn(this);
    }

    public void SetIsBusy(bool isBusy)
    {
        _isBusy = isBusy;
    }

    public void OnDefenderDied()
    {
        SetIsBusy(false);
    }

    public void InitHighlightPanel(GameObject panel)
    {
        _highlightPanel = panel;
    }
}