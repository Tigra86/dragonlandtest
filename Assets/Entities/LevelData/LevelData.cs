﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New entities", menuName = "Create New Level Data")]
public class LevelData : ScriptableObject
{
    [SerializeField] private int _currentLevel;
    [SerializeField] private int _maxAvailableLevel;

    public int CurrentLevel => _currentLevel;
    public int MaxAvailableLevel => _maxAvailableLevel;

    public void IncreaseCurrentLevel()
    {
        _currentLevel++;
    }
}
